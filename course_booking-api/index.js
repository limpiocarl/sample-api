const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/uploads", express.static("uploads"));

mongoose.connect(
  "mongodb+srv://admin:admin131@zuittbootcamp.0e0ak.mongodb.net/course-booking?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", () => console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`Server running on port ${port}`));

// https://polar-ravine-90941.herokuapp.com/
