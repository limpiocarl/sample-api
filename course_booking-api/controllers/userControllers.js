const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require('../models/Course')



// checking if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;

		} else {

			return false;

		};
	});

};

// user registration

module.exports.registerUser = (reqBody) => {

			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNo : reqBody.mobileNo,
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {

				if (error) {

					return false;

				} else {

					return true;

		};

	});

};



// login route

module.exports.loginUser = (reqBody) => {
		
	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false;

		} else {

		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return { access : auth.createAccessToken(result) }

			} else {

				return false;

			};

		};

	});

};


// get details

module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};

// enroll

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId})

		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return user
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error) => {
			if(error){
				return false
			} else {
				return course
			}
		})
	})

	if(isUserUpdated && isCourseUpdated){
		return true
	} else {
		return false
	}
};
